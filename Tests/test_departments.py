'''
Created on 4 Jan 2021

@author: Customer
'''
import unittest
from domain.departments import MyDepartments
from domain.patients import MyPatients


class TestDepartments(unittest.TestCase):


    def setUp(self):
        ''' 
        this function will be run at the beginning of each test method
        '''
        self.__p1=MyPatients("Daniela", "Popita", "6020304019548", "disease" )
        self.__p2=MyPatients("Daria", "Sertan", "6022121097451", "dis" )
        self.__p3=MyPatients("Ion", "Baciu", "1068072254896", "fever" )
        self.__p4=MyPatients("Alin", "Esca", "5098030534578", "infection" )
        self.__p5=MyPatients("Iosif", "Plers", "1054072254896", "burn" )
        self.__p6=MyPatients("Ioana", "Fecheer", "2058072254896", "COVID-19")
        
        self.__d1=MyDepartments("1","COLD", 200)
        self.__d1.add_patient(self.__p1)
        self.__d1.add_patient(self.__p2)
        self.__d1.add_patient(self.__p3)
        self.__d1.add_patient(self.__p6)

        self.__d2=MyDepartments("2","INFECTIONS",150)
        self.__d2.add_patient(self.__p2)
        self.__d2.add_patient(self.__p4)
        self.__d2.add_patient(self.__p5)
        
        self.__emptyd=MyDepartments("0","Priority",23)

    def tearDown(self):
        ''' 
        this function will be run at the end of all the test methods
        '''
        unittest.TestCase.tearDown(self)

    def test_create(self):
        '''
        Testing the departement class' creation
        '''
                #lenght
        self.assertEqual(len(self.__d1),4)
        self.assertEqual(len(self.__d2),3)

        #string representation
        self.assertEqual(str(self.__d1),"Department of id 1 of name COLD has 200 beds and the following patients:" + '\n' +"Patient with numerical code 6020304019548 has first name Daniela last name Popita and disease disease"+'\n'+"Patient with numerical code 6022121097451 has first name Daria last name Sertan and disease dis"+'\n'+"Patient with numerical code 1068072254896 has first name Ion last name Baciu and disease fever"+'\n'+"Patient with numerical code 2058072254896 has first name Ioana last name Fecheer and disease COVID-19"+'\n')
        self.assertEqual(str(self.__emptyd),"Department of id 0 of name Priority has 23 beds and no patients")
        
        #add
        self.assertRaises(ValueError,self.__d1.add_patient,MyPatients("Ana","Pesr","1068072254896","dis"))
        
        #getters
        self.assertEqual(self.__d1.get_department_id(),"1")
        self.assertEqual(self.__d2.get_department_id(),"2")
        self.assertEqual(self.__emptyd.get_department_id(),"0")
        
        self.assertEqual(self.__d1.get_department_name(),"COLD")
        self.assertEqual(self.__d2.get_department_name(),"INFECTIONS")
        self.assertEqual(self.__emptyd.get_department_name(),"Priority")

        self.assertEqual(self.__d1.get_department_number_of_beds(),200)
        self.assertEqual(self.__d2.get_department_number_of_beds(),150)
        self.assertEqual(self.__emptyd.get_department_number_of_beds(),23)

        self.assertEqual(self.__d1.get_list_of_patients(),[self.__p1,self.__p2,self.__p3,self.__p6])
        self.assertEqual(self.__d2.get_list_of_patients(),[self.__p2,self.__p4,self.__p5])
        self.assertEqual(self.__emptyd.get_list_of_patients(),[])
        
        #setters
        self.__d1.set_department_id("4")
        self.assertEqual(self.__d1.get_department_id(),"4")
        self.__d2.set_department_id("1")
        self.assertEqual(self.__d2.get_department_id(),"1")
        self.__emptyd.set_department_id("4")
        self.assertEqual(self.__emptyd.get_department_id(),"4")

        self.__d1.set_department_name("Name 1")
        self.assertEqual(self.__d1.get_department_name(),"Name 1")
        self.__d2.set_department_name("Name 2")
        self.assertEqual(self.__d2.get_department_name(),"Name 2")
        self.__emptyd.set_department_name("Name 3")
        self.assertEqual(self.__emptyd.get_department_name(),"Name 3")

        self.__d1.set_department_number_of_beds(1)
        self.assertEqual(self.__d1.get_department_number_of_beds(),1)
        self.__d2.set_department_number_of_beds(2)
        self.assertEqual(self.__d2.get_department_number_of_beds(),2)
        self.__emptyd.set_department_number_of_beds(3)
        self.assertEqual(self.__emptyd.get_department_number_of_beds(),3)

        
        self.__d1.set_list_of_patients([self.__p3,self.__p2,self.__p5])
        self.assertEqual(self.__d1.get_list_of_patients(),[self.__p3,self.__p2,self.__p5])
        
    def test_get_at_index(self):
        '''
        Testing the function get_at_index
        '''
        self.assertEqual(self.__d1.get_at_index(1),self.__p2)
        self.assertEqual(self.__d1.get_at_index(0),self.__p1)
        self.assertEqual(self.__d1.get_at_index(3),self.__p6)
        self.assertEqual(self.__d2.get_at_index(1),self.__p4)
        self.assertEqual(self.__d2.get_at_index(0),self.__p2)

        self.assertRaises(IndexError,self.__d1.get_at_index,30)
        self.assertRaises(IndexError,self.__d2.get_at_index,-2)
        self.assertRaises(ValueError,self.__emptyd.get_at_index,1)

    def test_delete_by_index(self):
        '''
        Testing the function delete_by_index
        '''

        self.__d1.delete_by_index(0)
        self.assertEqual(len(self.__d1),3)
        
        self.__d1.delete_by_index(0)
        self.assertEqual(len(self.__d1),2)
        
        self.assertRaises(IndexError,self.__d2.delete_by_index,-2)
        self.assertRaises(ValueError,self.__emptyd.delete_by_index,1)

    def test_update_patient_at_a_given_index(self):
        '''
        Testing the function update_patient_at_a_given_index
        '''        

        self.__d1.update_patient_at_a_given_index(0, "Ana", "Maria", "fever")
        self.assertEqual(self.__d1.get_at_index(0).get_first_name(),"Ana")
        self.assertEqual(self.__d1.get_at_index(0).get_last_name(),"Maria")
        self.assertEqual(self.__d1.get_at_index(0).get_disease(),"fever")

        self.assertRaises(IndexError,self.__d2.update_patient_at_a_given_index,-1,"Ana", "Maria", "fever")
        self.assertRaises(ValueError,self.__emptyd.update_patient_at_a_given_index,2,"Ana", "Maria", "fever")

    def test_sort_patients_by_personal_numerical_code(self):
        '''
        Testing the function sort_patients_by_personal_numerical_code
        '''
        
        self.__d1.sort_patients_by_personal_numerical_code()
        self.assertEqual(self.__d1.get_list_of_patients(),[self.__p3,self.__p6,self.__p1,self.__p2])
       
        self.__d2.sort_patients_by_personal_numerical_code()
        self.assertEqual(self.__d2.get_list_of_patients(),[self.__p5,self.__p4,self.__p2])
        
        self.assertRaises(ValueError, self.__emptyd.sort_patients_by_personal_numerical_code)
        
    def test_sort_patients_alphabetically(self):
        '''
        Testing the function sort_patients_alphabetically
        '''        
        
        self.__d1.sort_patients_alphabetically()
        self.assertEqual(self.__d1.get_list_of_patients(),[self.__p3,self.__p6,self.__p1,self.__p2])
       
        self.__d2.sort_patients_alphabetically()
        self.assertEqual(self.__d2.get_list_of_patients(),[self.__p4,self.__p5,self.__p2])
 
        self.assertRaises(ValueError, self.__emptyd.sort_patients_alphabetically)

    def test_get_number_of_patients_of_age_greater_than(self):
        '''
        Testing the function get_number_of_patients_of_age_greater_than
        '''        

        self.assertEqual(self.__d1.get_number_of_patients_of_age_greater_than(20),2)
        self.assertEqual(self.__d2.get_number_of_patients_of_age_greater_than(20),1)
        
    def test_department_has_patients_age_less_than(self):
        '''
        Testing the function department_has_patients_age_less_than
        '''        
        self.assertTrue(self.__d1.department_has_patients_age_less_than(20))
        self.assertTrue(self.__d2.department_has_patients_age_less_than(120))
        self.assertFalse(self.__d2.department_has_patients_age_less_than(10))
        self.assertFalse(self.__emptyd.department_has_patients_age_less_than(12))
        
    def test_patients_with_given_string(self):
        '''
        Testing the function patients_with_given_string
        '''        
        self.assertEqual(self.__d1.patients_with_given_string("Da"),[self.__p1,self.__p2])
        self.assertEqual(self.__d2.patients_with_given_string("si"),[self.__p5])

    def test_patients_with_given_name(self):
        '''
        Testing the function patients_with_given_name
        '''        
        self.assertTrue(self.__d1.patients_with_given_name("Daniela"))
        self.assertTrue(self.__d2.patients_with_given_name("Iosif"))
        self.assertFalse(self.__d1.patients_with_given_name("Iosif"))
        self.assertFalse(self.__emptyd.patients_with_given_name("Ana"))


if __name__ == "__main__":
    unittest.main()