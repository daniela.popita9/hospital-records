'''
Created on 4 Jan 2021

@author: Customer
'''
import unittest
from domain.departments import MyDepartments
from domain.patients import MyPatients
from infrastructure.departments_repo import DepartmentsRepository

class Test(unittest.TestCase):


    def setUp(self):
        ''' 
        this function will be run at the beginning of each test method
        '''
        self.__p1=MyPatients("Daniela", "Popita", "6020304019548", "disease" )#age 18
        self.__p2=MyPatients("Daria", "Sertan", "6022121097451", "dis" ) #age 18
        self.__p3=MyPatients("Ion", "Baciu", "1868072254896", "fever" ) #age 34
        self.__p4=MyPatients("Alin", "Esca", "5098030534578", "infection" ) # age 11
        self.__p5=MyPatients("Iosif", "Plers", "1984072254896", "burn" )# age 22
        self.__p6=MyPatients("Ioana", "Fecheer", "2758072254896", "COVID-19")# age 45
        self.__p7=MyPatients("Antonio", "Renef", "6058072254896", "COVID-19")# age 15
        
        
        self.__d1=MyDepartments("1","COLD", 200)
        self.__d1.add_patient(self.__p1)
        self.__d1.add_patient(self.__p2)
        self.__d1.add_patient(self.__p3)
        self.__d1.add_patient(self.__p6)
        self.__d1.add_patient(self.__p7)


        self.__d2=MyDepartments("2","INFECTIONS",150)
        self.__d2.add_patient(self.__p2)
        self.__d2.add_patient(self.__p4)
        self.__d2.add_patient(self.__p5)
        
        self.__emptyd=MyDepartments("0","Priority",23)
        
        self.__wrongd=MyDepartments("2","Iron Deficenty",309)
        self.__wrongd.add_patient(self.__p1)
        
        self.__h=DepartmentsRepository()
        self.__h.add_department(self.__d1)
        self.__h.add_department(self.__d2)
        self.__h.add_department(self.__emptyd)
        
        self.__eh=DepartmentsRepository()

    def tearDown(self):
        ''' 
        this function will be run at the end of all the test methods
        '''
        unittest.TestCase.tearDown(self)

    def testCreate(self):
        '''
        Testing the department repository's creation
        '''      
        #add
        self.assertRaises(ValueError,self.__h.add_department,self.__wrongd)
        
        #lenght
        self.assertEqual(len(self.__h),3)
        
        #string representation
        self.assertEqual(str(self.__h),"The hospital has the following departments: "+'\n'+"Department of id 1 of name COLD has 200 beds and the following patients:"+'\n'+"Patient with numerical code 6020304019548 has first name Daniela last name Popita and disease disease"+'\n'+"Patient with numerical code 6022121097451 has first name Daria last name Sertan and disease dis"+'\n'+"Patient with numerical code 1868072254896 has first name Ion last name Baciu and disease fever"+'\n'+"Patient with numerical code 2758072254896 has first name Ioana last name Fecheer and disease COVID-19"+'\n'+"Patient with numerical code 6058072254896 has first name Antonio last name Renef and disease COVID-19"+'\n'+'\n'+"Department of id 2 of name INFECTIONS has 150 beds and the following patients:"+'\n'+"Patient with numerical code 6022121097451 has first name Daria last name Sertan and disease dis"+'\n'+"Patient with numerical code 5098030534578 has first name Alin last name Esca and disease infection"+'\n'+"Patient with numerical code 1984072254896 has first name Iosif last name Plers and disease burn"+'\n'+'\n'+"Department of id 0 of name Priority has 23 beds and no patients"+'\n')
        self.assertEqual(str(self.__eh),"This hospital has no departments")
        
        #getters
        self.assertEqual(self.__h.get_list_of_departments(),[self.__d1,self.__d2,self.__emptyd])
        self.assertEqual(self.__eh.get_list_of_departments(),[])
        
    def test_get_department_at_index(self):
        '''
        Testing the function get_department_at_index
        '''
        self.assertEqual(self.__h.get_department_at_index(0),self.__d1)
        self.assertEqual(self.__h.get_department_at_index(1),self.__d2)
        self.assertEqual(self.__h.get_department_at_index(2),self.__emptyd)
        self.assertRaises(IndexError,self.__h.get_department_at_index,4)
        self.assertRaises(IndexError,self.__h.get_department_at_index,-1)
        self.assertRaises(ValueError,self.__eh.get_department_at_index,1)
    
    def test_delete_by_index(self):
        '''
        Testing the function delete_by_index
        '''
        self.assertRaises(IndexError,self.__h.delete_by_index,4)
        self.assertRaises(IndexError,self.__h.delete_by_index,-1)
        self.assertRaises(ValueError,self.__eh.delete_by_index,1)
        
        self.__h.delete_by_index(0)
        self.assertEqual(len(self.__h),2)
        self.__h.delete_by_index(1)
        self.assertEqual(len(self.__h),1)
        self.__h.delete_by_index(0)        
        self.assertEqual(len(self.__h),0)

    def test_update_department_at_a_given_index(self):
        '''
        Testing the function update_department_at_a_given_index
        '''
        self.__h.update_department_at_a_given_index(0, "UPDATE", 9)
        self.assertEqual(self.__d1.get_department_name(),"UPDATE")
        self.assertEqual(self.__d1.get_department_number_of_beds(),9)
        
        self.assertRaises(IndexError,self.__h.update_department_at_a_given_index,4, "UPDATE", 9)
        self.assertRaises(IndexError,self.__h.update_department_at_a_given_index,-1, "UPDATE", 9)
        self.assertRaises(ValueError,self.__eh.update_department_at_a_given_index,1, "UPDATE", 9)

    def test_sort_departments_by_number_of_patients(self):
        '''
        Testing the function sort_departments_by_number_of_patients
        '''
        self.__h.sort_departments_by_number_of_patients()
        self.assertEqual(self.__h.get_list_of_departments(),[self.__emptyd,self.__d2,self.__d1])
        self.assertRaises(ValueError,self.__eh.sort_departments_by_number_of_patients)
        
    def test_sort_departments_by_patients_with_age_greater_than(self):
        '''
        Testing the function sort_departments_by_patients_with_age_greater_than
        '''
        self.__h.sort_departments_by_patients_with_age_greater_than(30)
        self.assertEqual(self.__h.get_list_of_departments(),[self.__emptyd,self.__d2,self.__d1])
        self.assertRaises(ValueError,self.__eh.sort_departments_by_patients_with_age_greater_than,23)

    def test_sort_departments_and_patients(self):
        '''
        Testing the function sort_departments_by_patients_with_age_greater_than
        '''
        self.__h.sort_departments_and_patients()
        self.assertEqual(self.__h.get_list_of_departments(),[self.__emptyd,self.__d2,self.__d1])
        self.assertEqual(self.__d1.get_list_of_patients(),[self.__p3,self.__p6,self.__p1,self.__p7,self.__p2])
        self.assertEqual(self.__d2.get_list_of_patients(),[self.__p4,self.__p5,self.__p2])
        self.assertRaises(ValueError,self.__eh.sort_departments_by_patients_with_age_greater_than,23)

    def test_get_departments_with_patients_under_age(self):
        '''
        Testing the function get_departments_with_patients_under_age
        '''
        self.assertRaises(ValueError,self.__eh.get_departments_with_patients_under_age,23)
        self.assertEqual(self.__h.get_departments_with_patients_under_age(10),"There are no departments with patients that have age less than 10")
        
    def test_get_departments_patients_with_first_name(self):
        '''
        Testing the function get_departments_patients_with_first_name
        '''
        
        self.assertRaises(ValueError,self.__eh.get_departments_patients_with_first_name,"Antonio")
        self.assertEqual(self.__h.get_departments_patients_with_first_name("Ana"),"There are no departments with patients that have the name Ana")
        

if __name__ == "__main__":
    unittest.main()