'''
Created on 10 Dec 2020

@author: Customer
'''
from domain.patients import MyPatients
import unittest


class Test(unittest.TestCase):


    def setUp(self):
        ''' 
        this function will be run at the beginning of each test method
        '''
        self.__p1=MyPatients("Daniela", "Popita", "6020304019548", "disease" )
        self.__p2=MyPatients("Daria", "Sertan", "2822121097451", "dis" )


    def tearDown(self):
        ''' 
        this function will be run at the end of all the test methods
        '''
        unittest.TestCase.tearDown(self)


    def test_create(self):
        '''
        Testing the MyPatients class creation
        '''
        self.assertRaises(ValueError,MyPatients,"Daniela", "Popita", "60203040195", "disease")

        #getters
        self.assertEqual(self.__p1.get_disease(),"disease")
        self.assertEqual(self.__p2.get_disease(),"dis")
        self.assertEqual(self.__p1.get_first_name(),"Daniela")
        self.assertEqual(self.__p2.get_first_name(),"Daria")
        self.assertEqual(self.__p1.get_last_name(),"Popita")
        self.assertEqual(self.__p2.get_last_name(),"Sertan")
        self.assertEqual(self.__p1.get_personal_numerical_code(),"6020304019548")
        self.assertEqual(self.__p2.get_personal_numerical_code(),"2822121097451")
        
        #setters
        self.__p1.set_disease("nodisease")
        self.__p1.set_first_name("Ana")
        self.__p1.set_last_name("Fegre")
        self.__p1.set_personal_numerical_code("6030304019548")
        self.assertRaises(ValueError,self.__p1.set_personal_numerical_code,"602030416330")
        self.assertEqual(self.__p1.get_disease(),"nodisease")
        self.assertEqual(self.__p1.get_first_name(),"Ana")
        self.assertEqual(self.__p1.get_last_name(),"Fegre")
        self.assertEqual(self.__p1.get_personal_numerical_code(),"6030304019548")
        
        #string representation#
        self.assertEqual(str(self.__p1),"Patient with numerical code 6030304019548 has first name Ana last name Fegre and disease nodisease")
        self.assertEqual(str(self.__p2),"Patient with numerical code 2822121097451 has first name Daria last name Sertan and disease dis")
        
    def test_get_age(self):
        '''   
            Testing the function get_age
        '''
        self.assertEqual(self.__p1.get_age(),18)
        self.assertEqual(self.__p2.get_age(),38)


if __name__ == "__main__":
    unittest.main()