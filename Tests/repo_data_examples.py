'''
Created on 6 Jan 2021

@author: Customer
'''
from domain.departments import MyDepartments
from domain.patients import MyPatients
from infrastructure.departments_repo import DepartmentsRepository
def data_examples():
    '''    
    Data examples for the departments repository    
    '''
    p1=MyPatients("Daniela", "Popita", "6020304019548", "disease" )
    p2=MyPatients("Daria", "Sertan", "6022121097451", "dis" )
    p3=MyPatients("Ion", "Baciu", "1068072254896", "fever" )
    p4=MyPatients("Alin", "Esca", "5098030534578", "infection" )
    p5=MyPatients("Iosif", "Plers", "1054072254896", "burn" )
    p6=MyPatients("Ioana", "Fecheer", "2058072254896", "COVID-19" )

    d1=MyDepartments("1","COLD", 200)
    d1.add_patient(p1)
    d1.add_patient(p2)
    d1.add_patient(p3)
    d1.add_patient(p6)
    
    d2=MyDepartments("2","INFECTIONS",150)
    d2.add_patient(p2)
    d2.add_patient(p4)
    d2.add_patient(p5)
     
    h1=DepartmentsRepository()
    h1.add_department(d1)
    h1.add_department(d2)
     
    d=MyDepartments("1","COLD", 200)
    h=DepartmentsRepository()
    h.add_department(d)


    #testing the creation, lenght function, and the string representation of the departments repository with data exemples
    print("Tests for the creation, lenght function, and the string representation of the departments repository with data exemples:")
    print()

    print(h,end=" ")
    print("Lenght: ",len(h))
    print()
    print(h1,end=" ")
    print("Lenght: ",len(h1))
    print()

    #testing the departments repository function get at index
    
    print("testing the departments repository function get_department_at_index with data examples: ")
    print()
    print("The repository is: ", h1)
    print("The department at index 0: ",h1.get_department_at_index(0))
    print("The department at index 1: ",h1.get_department_at_index(1))
    print()

    #testing the departments repository function delete_by_index
    
    print("testing the departments repository function delete_by_index with data examples: ")
    print()
    print("The repository before deleting is : ", h1)

    h1.delete_by_index(1)
    print("The repository after deleting the department at index 1 is : ", h1)

    h1.delete_by_index(0)
    print("The repository after deleting the department at index 0 is : ", h1)
    print()


    h1.add_department(d1)
    h1.add_department(d2)

    #testing the departments repository function update_department_at_a_given_index
    
    print("testing the departments repository function update_department_at_a_given_index with data examples: ")
    print()
    print("The repository before updates is : ", h1)

    h1.update_department_at_a_given_index(0, "first_name1", 20)
    print("The repository after first update, the department at index 0 is : ", h1)

    h1.update_department_at_a_given_index(1, "first_name2", 30)
    print("The repository after second update, the department at index 1 is : ", h1)

    print()

    #testing the departments repository function sort_departments_by_number_of_patients
    
    print("testing the departments repository function sort_departments_by_number_of_patients with data examples: ")
    print()
    print("The repository before sort is : ", h1)

    h1.sort_departments_by_number_of_patients()
    
    print("The repository after sort is : ", h1)
    print()

    #testing the departments repository function sort_departments_by_patients_with_age_greater_than
    de=MyDepartments(50,"NAME",300)
    de.add_patient(p1)
    de.add_patient(p2)
    h.add_department(de)
    h.add_department(d2)


    print("testing the departments repository function sort_departments_by_patients_with_age_greater_than with data examples: ")
    print()
    print("The repository before sort is : ", h)

    h.sort_departments_by_patients_with_age_greater_than(90)
    
    print("The repository after sort is : ", h)
    print()

    #testing the departments repository function get_departments_with_patients_under_age
    
    print("testing the departments repository function get_departments_with_patients_under_age with data examples: ")
    print()
    print("The repository is : ", h1)
    
    print( "The departments with patients under age greater than 10 are:", h1.get_departments_with_patients_under_age(10))
    print( "The departments with patients under age greater than 100 are:", h1.get_departments_with_patients_under_age(100))

    print()
    
    #testing the departments repository function get_departments_patients_with_first_name
    
    print("testing the departments repository function get_departments_patients_with_first_name with data examples: ")
    print()
    print("The repository is : ", h1)
    
    print( "The departments with patients that have first name (Daniela) are: ", h1.get_departments_patients_with_first_name("Daniela"))
    print( "The departments with patients that have first name (Alin):are:", h1.get_departments_patients_with_first_name("Alin"))
    print( "The departments with patients that have first name (Flavius):are:", h1.get_departments_patients_with_first_name("Flavius"))

    print()
    print("Tests ended succesfully!")

data_examples()
