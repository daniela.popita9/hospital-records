'''
Created on 6 Jan 2021

@author: Customer
'''
from domain.departments import MyDepartments
from domain.patients import MyPatients
def data_examples():
    '''
    Data examples for the module departments
    '''

    p1=MyPatients("Daniela", "Popita", "6020304019548", "disease" )
    p2=MyPatients("Daria", "Sertan", "6022121097451", "dis" )
    p3=MyPatients("Ion", "Baciu", "1068072254896", "fever" )
    p4=MyPatients("Alin", "Esca", "5098030534578", "infection" )
    p5=MyPatients("Iosif", "Plers", "1054072254896", "burn" )
    p6=MyPatients("Ioana", "Fecheer", "2058072254896", "COVID-19" )

    d1=MyDepartments("1","COLD", 200)
    d1.add_patient(p1)
    d1.add_patient(p2)
    d1.add_patient(p3)
    d1.add_patient(p6)

     
    d2=MyDepartments("2","INFECTIONS",150)
    d2.add_patient(p2)
    d2.add_patient(p4)
    d2.add_patient(p5)
     
    #testing the creation, lenght function, and the string representation of the departments class with data exemples
    print("Tests for the creation, lenght function, and the string representation of the departments class with data exemples:")
    print(d1)
    print("Lenght: ",len(d1))
    print()
    print(d1)
    print("Lenght: ",len(d2))
    print()
    
    #testing the departments class getters and setters with data examples
    print("testing the departments class getters and setters with data examples: ")
    print()
    print("Department's atributes before: ")
    print("Id ",d1.get_department_id())
    print("Name ",d1.get_department_name())
    print("Number of beds ",d1.get_department_number_of_beds())

    d1.set_department_name("New name")
    d1.set_department_id(20)
    d1.set_department_number_of_beds(300)
    
    print()
    print("Department's atributes after: ")
    print("Id ",d1.get_department_id())
    print("Name ",d1.get_department_name())
    print("Number of beds ",d1.get_department_number_of_beds())
    print()
    
    #testing the departments class function get at index
    
    print("testing the departments class function get at index with data examples: ")
    print()
    print("The department is: ", d2)
    print("The patient at index 0: ",d2.get_at_index(0))
    print("The patient at index 1: ",d2.get_at_index(1))
    print("The patient at index 2: ",d2.get_at_index(2))
    print()

    #testing the departments class function delete_by_index
    
    print("testing the departments class function delete_by_index with data examples: ")
    print()
    print("The department before deleting is : ", d2)

    d2.delete_by_index(2)
    print("The department after deleting the patient at index 2 is : ", d2)

    d2.delete_by_index(1)
    print("The department after deleting the patient at index 1 is : ", d2)

    d2.delete_by_index(0)
    print("The department after deleting the patient at index 0 is : ", d2)
    print()
    
    #testing the departments class function update_patient_at_a_given_index
    
    print("testing the departments class function update_patient_at_a_given_index with data examples: ")
    print()
    print("The department before updates is : ", d1)

    d1.update_patient_at_a_given_index(2, "first_name1", "last_name1", "disease1")
    print("The department after first update, the patient at index 2 is : ", d1)

    d1.update_patient_at_a_given_index(1, "first_name2", "last_name2", "disease2")
    print("The department after second update, the patient at index 1 is : ", d1)

    d1.update_patient_at_a_given_index(0,"first_name3", "last_name3", "disease3")
    print("The department after third update, the patient at index 0 is : ", d1)
    print()
    
    #testing the departments class function sort_patients_by_personal_numerical_code
    
    print("testing the departments class function sort_patients_by_personal_numerical_code with data examples: ")
    print()
    print("The department before sort is : ", d1)

    d1.sort_patients_by_personal_numerical_code()
    
    print("The department after sort is : ", d1)
    print()

    #testing the departments class function sort_patients_alphabetically
    
    print("testing the departments class function sort_patients_alphabetically with data examples: ")
    print()
    print("The department before sort is : ", d1)

    d1.sort_patients_alphabetically()
    
    print("The department after sort is : ", d1)
    print()

    #testing the departments class function get_number_of_patients_of_age_greater_than
    
    print("testing the departments class function get_number_of_patients_of_age_greater_than with data examples: ")
    print()
    print("The department is : ", d1)
    
    print( "The number of patients with age greater than 10 are:",d1.get_number_of_patients_of_age_greater_than(10))
    print( "The number of patients with age greater than 10 are:",d1.get_number_of_patients_of_age_greater_than(100))

    print()

    #testing the departments class function department_has_patients_age_less_than
    
    print("testing the departments class function department_has_patients_age_less_than with data examples: ")
    print()
    print("The department is : ", d1)
    
    print( "Are patients with age less than 10?:",d1.department_has_patients_age_less_than(10))
    print( "Are patients with age less than 100?:",d1.department_has_patients_age_less_than(100))

    print()


    #testing the departments class function department_has_patients_age_less_than
    
    print("testing the departments class function patients_with_given_string with data examples: ")
    print()
    print("The department is : ", d1)
    
    print( "Are patients with given string (an)?:",len(d1.patients_with_given_string("an")))
    print( "Are patients with given string (iu)?:",d1.patients_with_given_string("iu"))

    print()

    #testing the departments class function patients_with_given_name
    
    print("testing the departments class function patients_with_given_name with data examples: ")
    print()
    print("The department is : ", d1)
    
    print( "Are patients with given name (Daniela)?:",d1.patients_with_given_name("Daniela"))
    print( "Are patients with given name (Ioana)?:",d1.patients_with_given_name("Ioana"))

    print()
    print("Tests ended succesfully!")
data_examples()
    
