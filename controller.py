'''
Created on 4 Jan 2021

@author: Customer
'''
from infrastructure.departments_repo import DepartmentsRepository as DR
from domain.departments import MyDepartments

class Controller():
    '''
    The controller is a class that manages the Departments repository
    '''


    def __init__(self, hospitaL: DR=DR()):
        '''
        Constructor for the Controller class
        '''
        self.__repo=hospitaL
    
    def create_dep(self, dep_id, name, number_of_beds):
        '''
        Creates a department and adds it to the controller
        '''
        created_dep=MyDepartments(dep_id, name, number_of_beds)
        self.__repo.add_department(created_dep)
        
    def print_hospital(self):
        '''
        Prints the controller
        '''
        return str(self.__repo)
    
    def print_department(self,dep_index):
        '''
        Prints the department situated at the index INDEX in the controller
        index- a number
        '''
        return str(self.__repo.get_department_at_index(dep_index))
    
    def get_department_at_index(self,dep_index): 
        '''
        Returns the department situated at the index INDEX in the controller
        index- a number
        '''
        return self.__repo.get_department_at_index(dep_index)
    
    def add_patient_to_dep(self,dep_index,patient):
        '''
        Adds a patient to  the department situated at the index dep_index in the controller
        dep_index- a numbe that is the department's index
        patient- the patient to add
        '''
        return self.__repo.get_department_at_index(dep_index).add_patient(patient)
        
    def get_patients_from_dep_given_by_index(self,dep_index):
        '''
        Returns the patients from the department situated at the index dep_index in the controller
        dep_index- a numbe that is the department's index
        '''
        return self.__repo.get_department_at_index(dep_index).get_list_of_patients()
    
    def get_a_patient_by_index_from_dep_given_by_index(self,dep_index,patient_index):
        '''
        Returns the patient at te index patient_index from the department situated at the index dep_index in the controller
        dep_index- a number that is the department's index
        patient_index- a number that is the patient's index
        '''
        return self.__repo.get_department_at_index(dep_index).get_at_index(patient_index)
    
    def update_a_patient_by_index_from_dep_given_by_index(self, dep_index, patient_index, new_first_name, new_last_name, new_disease):
        '''
        Controller command for the function update_patient_at_a_given_index
        '''
        return self.__repo.get_department_at_index(dep_index).update_patient_at_a_given_index(patient_index, new_first_name, new_last_name, new_disease)

    def delete_a_patient_by_index_from_dep_given_by_index(self,dep_index,patient_index):
        '''
        Controller command for the function delete_by__index
        '''
        self.__repo.get_department_at_index(dep_index).delete_by_index(patient_index)

    def sort_patients_by_personal_numerical_code_from_dep_given_by_index(self,dep_index):
        '''
        Controller command for the function sort_patients_by_personal_numerical_code
        '''
        self.__repo.get_department_at_index(dep_index).sort_patients_by_personal_numerical_code()
    
    def sort_patients_alphabetically_from_dep_given_by_index(self,dep_index):
        '''
        Controller command for the function sort_patients_alphabetically
        '''
        self.__repo.get_department_at_index(dep_index).sort_patients_alphabetically()

    def identify_patients_with_given_string_from_dep_given_by_index(self,dep_index,g_string):
        '''
        Controller command for the function identify_patients_with_given_string_from_dep_given_by_index
        '''
        a= self.__repo.identify_patients_with_given_string_from_dep_given_by_index(dep_index, g_string)
        if a==None:
            return -1
        s=''
        for elem in a:
            s+=str(elem)+'\n'
        return s

    def update_a_dep_given_by_index(self, dep_index, new_name, new_beds_nr):
        '''
        Controller command for the function update_department_at_a_given_index
        '''
        self.__repo.update_department_at_a_given_index(dep_index, new_name, new_beds_nr)

    def delete_a_dep_given_by_index(self,dep_index,):
        '''
        Controller command for the function delete_by_index
        '''
        self.__repo.delete_by_index(dep_index)
    
    def sort_departments_by_number_of_patients(self):
        '''
        Controller command for the function sort_departments_by_number_of_patients
        '''

        self.__repo.sort_departments_by_number_of_patients()
    
    def sort_departments_and_patients(self):
        '''
        Controller command for the function sort_departments_and_patients
        '''
        self.__repo.sort_departments_and_patients()
    
    def sort_dep_by_age(self,age):
        '''
        Controller command for the function sort_departments_by_patients_with_age_greater_than
        '''
        self.__repo.sort_departments_by_patients_with_age_greater_than(age)
    
    def identify_departmen_patients_under_given_age(self,age):
        '''
        Controller command for the function get_departments_with_patients_under_age
        '''
        a= self.__repo.get_departments_with_patients_under_age(age)
        
        if a!="There are no departments with patients that have age less than "+str(age):
            s=''
            for elem in a.get_list_of_departments():
                s+=str(elem)+'\n'
            return s
        else:
            return a
    def identify_departments_patients_with_f_name(self,name):
        '''
        Controller command for the function get_departments_patients_with_first_name
        '''
        a= self.__repo.get_departments_patients_with_first_name(name)
        
        if a!="There are no departments with patients that have the name "+name:
            s=''
            for elem in a.get_list_of_departments():
                s+=str(elem)+'\n'
            return s
        else:
            return a