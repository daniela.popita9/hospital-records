'''
Created on 4 Jan 2021

@author: Customer
'''
from application.controller import Controller
from domain.patients import MyPatients
def print_Menu():
    '''
    Prints the menu
    '''
    msg = "Menu \n" 
    msg += "\t 1  - Create a department and add it to the hospital\n"
    msg += "\t 2  - Add a patient to a department at a certain index\n"
    msg += "\t 3  - Print the hospital\n"
    msg += "\t 4  - Get all patients from a depatment for which the index is given \n"
    msg += "\t 5  - Get the patient at a given index from a depatment for which the index is given \n"
    msg += "\t 6  - Update a patient at a given index from a depatment for which the index is given \n"
    msg += "\t 7  - Delete a patient by index from a depatment for which the index is given \n"
    msg += "\t 8  - Sort patients by personal numerical code  department, for which the index is given \n"
    msg += "\t 9  - Sort patients alphabetically from the department, for which the index is given \n"
    msg += "\t 10 - Identify  patients from a given department for which the first name or last name contain a given string\n"
    msg += "\t 11 - Get the department at a given index\n"
    msg += "\t 12 - Update a department at a given index\n"
    msg += "\t 13 - Delete a department by index\n"
    msg += "\t 14 - Sort departments by the number of patients\n"
    msg += "\t 15 - Sort departments by the number of patients having the age above a given limit\n"
    msg += "\t 16 - Sort departments by the number of patients and the patients in a department alphabetically\n"
    msg += "\t 17 - Identify departments where there are patients under a given age\n"
    msg += "\t 18 - Identify department/departments where there are patients with a given first name\n"

    print(msg)

def create_patient():
    '''
    Creates an object of type MyPatient 
    '''
    errors=0        
    f_name=input("Give the first name of the patient: ")
    l_name=input("Give the last name of the patient: ")
    
    try:
        num_code=int(input("Give the ipersonal numerical code of the patient: "))
    except ValueError as e:
        print("Error! The numerical personal code has to be a 13 digits number")
        errors=errors+1
            
    if errors==0:            
        
        disease=input("Give the disease of the patient: ")
        try:
            p=MyPatients(f_name, l_name, num_code, disease)
        except ValueError as e:
            print("Error! The numerical personal code has to be a 13 digits number")
            errors=errors+1
        
        if errors==0:            
            return p
    
    return -1
    
    
def interface():
    '''
    The interface for interacting with the user
    '''
    loop=True
    while loop:
        controller=Controller()
        print_Menu()
        command=input("Give the command: ")
        if command=="0":
            print("End of the program")
            break
        elif command=="1":
            errors=0
            dep_id=input("Give the id of the department: ")
            name=input("Give the name of the department: ")
            try:
                beds=int(input("Give the number of beds: "))
            except ValueError as e:
                print("Error, the number of beds has to be a number ")
                errors=errors+1
            if errors==0:
                try:
                    controller.create_dep(dep_id, name, beds)
                except ValueError as e:
                    print("Error! "+ str(e))
                
        elif command=="2":
            
            errors=0
            try:
                dep_index=int(input("Give the index of the department: "))
            except ValueError as e:
                print("Error! The index of the department has to be a number")
                errors=errors+1
            
            if errors==0:
                p=create_patient()
            
                if p!=-1 and errors==0:
            
                    try:
                        controller.add_patient_to_dep(dep_index, p)
                    except (ValueError, IndexError) as e:
                        print("Error!" +str(e))
                    
        elif command=="3":
            
            print(controller.print_hospital())
        
        elif command=="4":
            errors=0
            try:
                dep_index=int(input("Give the index of the department: "))
            except ValueError as e:
                print("Error! The index of the department has to be a number")
                errors= errors + 1

            try:
                print(controller.get_department_at_index(dep_index))
            except (ValueError, IndexError) as e:
                print("Error!" +str(e))
     
        elif command=="5":
            
            errors=0
            try:
                dep_index=int(input("Give the index of the department: "))
            except ValueError as e:
                print("Error! The index of the department has to be a number")
                errors= errors + 1
            
            if errors==0:
                try:
                    patient_index=int(input("Give the index of the patient: "))
                except ValueError as e:
                    print("Error! The index of the patient has to be a number")
                    errors= errors + 1
            
            if errors==0:
                
                try:
                    print("The patient from the department of index " + str(dep_index) + " that is situated at index " + str(patient_index) + " is: " + str(controller.get_a_patient_by_index_from_dep_given_by_index(dep_index, patient_index)))
                except (ValueError,IndexError) as e:
                    print("Error!" +str(e))

        elif command=="6":
            
            errors=0
            try:
                dep_index=int(input("Give the index of the department: "))
            except ValueError as e:
                print("Error! The index of the department has to be a number")
                errors= errors + 1
            
            if errors==0:
                try:
                    patient_index=int(input("Give the index of the patient: "))
                except ValueError as e:
                    print("Error! The index of the patient has to be a number")
                    errors= errors + 1
            
            if errors==0:                
                new_first_name=input("Give the new first name of the patient: ")
                new_last_name=input("Give the new last name of the patient: ")
                new_disease=input("Give the new disease of the patient: ")
                try:
                    print("The patient from the department of index " + str(dep_index) + " that is situated at index " + str(patient_index) + " updated is: " + str(controller.update_a_patient_by_index_from_dep_given_by_index(dep_index, patient_index, new_first_name, new_last_name, new_disease)))
                except (ValueError,IndexError) as e:
                    print("Error!" +str(e))

        elif command=="7":
            
            errors=0
            try:
                dep_index=int(input("Give the index of the department: "))
            except ValueError as e:
                print("Error! The index of the department has to be a number")
                errors= errors + 1
            
            if errors==0:
                try:
                    patient_index=int(input("Give the index of the patient: "))
                except ValueError as e:
                    print("Error! The index of the patient has to be a number")
                    errors= errors + 1
            
            if errors==0:
                
                try:
                    str(controller.delete_a_patient_by_index_from_dep_given_by_index(dep_index, patient_index))
                except (ValueError,IndexError) as e:
                    print("Error!" +str(e))
                    errors= errors + 1
            
            if errors==0:
                
                print("Patient deleted succesfully")

        elif command=="8":
            errors=0
            try:
                dep_index=int(input("Give the index of the department: "))
            except ValueError as e:
                print("Error! The index of the department has to be a number")
                errors= errors + 1
            
            if errors==0:
                
                try:
                    str(controller.sort_patients_by_personal_numerical_code_from_dep_given_by_index(dep_index))
                except (ValueError,IndexError) as e:
                    print("Error!" +str(e))
                    errors= errors + 1
                    
            if errors==0:
                
                print("Patients sorted succesfully, the department is now:  ",controller.print_department(dep_index))
        
        elif command=="9":
            errors=0
            try:
                dep_index=int(input("Give the index of the department: "))
            except ValueError as e:
                print("Error! The index of the department has to be a number")
                errors= errors + 1
            
            if errors==0:
                
                try:
                    str(controller.sort_patients_alphabetically_from_dep_given_by_index(dep_index))
                except (ValueError,IndexError) as e:
                    print("Error!" +str(e))
                    errors= errors + 1
                    
            if errors==0:
                
                print("Patients sorted succesfully, the department is now:  ",controller.print_department(dep_index))

        elif command=="10":
            errors=0
            try:
                dep_index=int(input("Give the index of the department: "))
            except ValueError as e:
                print("Error! The index of the department has to be a number")
                errors= errors + 1
            
            if errors==0:
                
                given_s=input("Give the string: ")
                
                try:
                    a=str(controller.identify_patients_with_given_string_from_dep_given_by_index(dep_index, given_s))
                except (ValueError,IndexError) as e:
                    print("Error!" +str(e))
                    errors= errors + 1
            if errors == 0: 
                
                if a=='-1':
                    print("There are no patients from the department of index " + str(dep_index) + " that have the string " + given_s)
                    
                else:
                    print("The patient from the department of index " + str(dep_index) + " that have the string " + given_s + " are : "+a)
                    
        elif command=="11":
            errors=0
            try:
                dep_index=int(input("Give the index of the department: "))
            except ValueError as e:
                print("Error! The index of the department has to be a number")
                errors= errors + 1
            if errors==0:
                
                try:
                    print("The department from the index " + str(dep_index) +" is: " + str(controller.get_department_at_index(dep_index)))
                except (ValueError,IndexError) as e:
                    print("Error!" +str(e))

        elif command=="12":
            
            errors=0
            try:
                dep_index=int(input("Give the index of the department: "))
            except ValueError as e:
                print("Error! The index of the department has to be a number")
                errors= errors + 1
                        
            if errors==0:                
                new_name=input("Give the new name of the department: ")
                new_beds_nr=int(input("Give the new number of beds of the department: "))
                try:
                    controller.update_a_dep_given_by_index(dep_index, new_name, new_beds_nr)
                except (ValueError,IndexError) as e:
                    print("Error!" +str(e))
                    errors= errors + 1
            
            if errors==0:                
                    print("The department of index " + str(dep_index) + " updated is: " + str(controller.get_department_at_index(dep_index)))
        
        elif command=="13":
            
            errors=0
            try:
                dep_index=int(input("Give the index of the department: "))
            except ValueError as e:
                print("Error! The index of the department has to be a number")
                errors= errors + 1
                        
            if errors==0:
                
                try:
                    controller.delete_a_dep_given_by_index(dep_index)
                except (ValueError,IndexError) as e:
                    print("Error!" +str(e))
                    errors= errors + 1
            
            if errors==0:
                
                print("Department deleted succesfully")
        
        elif command=="14":
            
            errors=0
            try:
                controller.sort_departments_by_number_of_patients()
            except (ValueError,IndexError) as e:
                print("Error!" +str(e))
                errors= errors + 1
                    
            if errors==0:
                
                print("Departments sorted succesfully, the hospital is now:  ",controller.print_hospital())


        elif command=="16":
            
            errors=0
            try:
                controller.sort_departments_and_patients()
            except (ValueError,IndexError) as e:
                print("Error!" +str(e))
                errors= errors + 1
                    
            if errors==0:
                
                print("Departments and patients sorted succesfully, the hospital is now:  ",controller.print_hospital())
        
        elif command=="15":
            
            errors=0
            try:
                age=int(input("Give the age: "))
            except (ValueError,IndexError) as e:
                print("Error! The aget has to be a positive number")
                errors= errors + 1

            try:
                controller.sort_dep_by_age(age)
            except (ValueError,IndexError) as e:
                print("Error!" +str(e))
                errors= errors + 1
                    
            if errors==0:
                
                print("Departments and patients sorted succesfully, the hospital is now:  ",controller.print_hospital())

        elif command=="17":
            
            errors=0
            try:
                age=int(input("Give the age: "))
            except (ValueError,IndexError) as e:
                print("Error! The aget has to be a positive number")
                errors= errors + 1

            try:
                a=controller.identify_departmen_patients_under_given_age(age)
            except (ValueError,IndexError) as e:
                print("Error!" +str(e))
                errors= errors + 1
                    
            if errors==0:
                
                print("Departments that have patients under age " +str(age) + " are: " + str(a))

        elif command=="18":
            
            errors=0
            given_n=input("Give the name: ")

            try:
                a=controller.identify_departments_patients_with_f_name(given_n)
            except (ValueError,IndexError) as e:
                print("Error!" +str(e))
                errors= errors + 1
                    
            if errors==0:
                
                print("Departments that have patients with fist name " +str(given_n) + " are: " + str(a))

        else:
            print("Command unknown")

