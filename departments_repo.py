'''
Created on 17 Dec 2020

@author: Customer
'''
from domain.departments import MyDepartments
from utils.sort_and_search import my_sort, my_search

class DepartmentsRepository():
    '''
    DepartmentsRepository is a class that manages all the departments from the class MyDepartments:
    -__departments_list- a list of objects of type MyDepartments - it is created in the class
                                            - the departments must have diffrent id
                                            - raises an error if the id of a department is already in the list of departments

    '''
    
    def __init__(self):
        '''
        Constructor for DepartmentsRepository: Creates a list of all the departments of class MyDepartments
        '''
        self.__departments_list=[]
    
    def add_department(self, department:MyDepartments):
        '''
        Adds an objet of type MyDepartments to the list of departments
        '''
        for d in self.__departments_list:
            if d.get_department_id() == department.get_department_id():
                raise ValueError ("The id of the departments has to be diffrent") 
        
        self.__departments_list.append(department)

    def get_list_of_departments(self):
        '''
        Getter method for the list of departments
        '''
        return self.__departments_list
    
    def set_list_of_departments(self,new_list):
        '''
        Setter method for the list of departments
        new_list- the new list of departments
        '''
        self.__departments_list=new_list
        
    def __len__(self):
        '''
        Returns the lenght of the list of departments
        '''
        return len(self.__departments_list)
    
    def __str__(self):
        '''
        Creates a string version of the list of departments
        '''
        
        if len(self.__departments_list)==0:
            s="This hospital has no departments"
        else:
            s='The hospital has the following departments: ' + '\n'
            for d in self.__departments_list:
                s+= str(d)+'\n'
        
        return s

    def get_department_at_index(self, index):
        '''
        Getter method: Gets the department located at a given index
        index- has to be greater or equal to 0 and less than the number of elements in the list
        Raises an error if the list is empoty or if the index is less than 0 or greater than the number of elements in the list
        '''
        if len(self.__departments_list)==0:
            raise ValueError("The list is empty")
         
        if index<0 or index>=len(self.__departments_list):
            raise IndexError("The index given does not exist")
        return self.__departments_list[index]
    
    def delete_by_index(self,index):
        '''
        Deletes a department at a given index
        index- has to be greater or equal to 0 and less than the number of elements in the list
        Raises an error if the list is empoty or if the index is less than 0 or greater than the number of elements in the list

        '''
        if len(self.__departments_list)==0:
            raise ValueError("The list is empty")
        
        if index<0 or index>=len(self.__departments_list):
            raise IndexError("The index given does not exist")
        del(self.__departments_list[index])

    def update_department_at_a_given_index(self, index, new_name, new_number_of_beds):
        '''
        - new_name- the new name of the department
                  - given as a string
        - new_number of beds - the new number of beds for the department
                     - given as a number
                     - raises an error if it is not a number
        - new_patients_list- a list of objects of type My patinets - the patients must have diffrent personal numerical codes
                                                                    - raises an error if the personal numerical code of a patients is already in the list of patients
  
        '''
        
        if len(self.__departments_list)==0:
            raise ValueError("The list is empty")
        
        elif index<0 or index>=len(self.__departments_list):
            raise IndexError("The index given does not exist")   
        
        elif new_number_of_beds<0:
            raise ValueError ("The number of beds has to be a positive number")
        
        self.__departments_list[index].set_department_number_of_beds(new_number_of_beds)
        self.__departments_list[index].set_department_name(new_name)
           
    def sort_departments_by_number_of_patients(self):
        '''
        Sorts the departments by the number of patients modifying the list
        Raises an error if the list is empty 
        Returns the departments list sorted
        '''
    
        if len(self.__departments_list)==0:
            raise ValueError("There are no departments")
 
        def relation_for_number_of_patients(dep1,dep2):
            return len(dep1)<len(dep2)
        
        my_sort(self.__departments_list,relation_for_number_of_patients)
    
    def sort_departments_by_patients_with_age_greater_than(self,age):
        '''
        Sorts the departments by the number of patients with age greater than age modifying the list
        Age- a number greater than 0
        Raises an error if the list is empty 
        '''
        if len(self.__departments_list)==0:
            raise ValueError("There are no departments")
 
        my_sort(self.__departments_list, lambda x,y: x.get_number_of_patients_of_age_greater_than(age)<y.get_number_of_patients_of_age_greater_than(age))
            
    def sort_departments_and_patients(self):
        '''
        Sorts the departments by the number of patients and the patients alphabetically modifying the list
        Raises an error if the list is empty 
        '''
        if len(self.__departments_list)==0:
            raise ValueError("There are no departments")
        
        self.sort_departments_by_number_of_patients()
        i=0
        while i<len(self.__departments_list):
            if len(self.__departments_list[i].get_list_of_patients()) != 0:
                self.__departments_list[i].sort_patients_alphabetically()
            i=i+1
        
    def get_departments_with_patients_under_age(self,age):
        '''
        Gets the departments that have patients with age under the age given, age
        Raises an error if the list is empty 
        Returns departments that have patients with age under the age given, age or a message if there are none
        '''
        if len(self.__departments_list)==0:
            raise ValueError("There are no departments")
                
        a=DepartmentsRepository()
        a.__departments_list=my_search(self.__departments_list,lambda x: x.department_has_patients_age_less_than(age)==True)
        if len(a)==0:
            return "There are no departments with patients that have age less than "+str(age)
        else:
            return a                
       
    def get_departments_patients_with_first_name(self,name):
        '''
        Gets the departments that have patients with the first name given, name
        name- the name we are looking for, a string
        Raises an error if the list is empty 
        Returns departments that have patients  with the first name given, name or a message if there are none
        '''
        if len(self.__departments_list)==0:
            raise ValueError("There are no departments")
        
        a=DepartmentsRepository()
        a.__departments_list= my_search(self.__departments_list,lambda x: x.patients_with_given_name(name)==True)
        if len(a)==0:
            return "There are no departments with patients that have the name "+name
        else:
            return a                

    def identify_patients_with_given_string_from_dep_given_by_index(self,dep_index,g_string):
        '''
        Auxiliar function for the function from the departments patients_with_given_string
        '''
        a=DepartmentsRepository()
        a.__departments_list=self.__departments_list[dep_index].patients_with_given_string(g_string)
        return a.get_list_of_departments()
    
