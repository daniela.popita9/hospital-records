'''
Created on 10 Dec 2020

@author: Customer
'''

class MyPatients():
    '''
    A n object of type MyPatients has the following atributes:
    - first_name- the patient's first name
                - given as a string
    - last_name- the patient's last name
                - given as a string
    - personal_numerical_code - a uique code for the patient
                              - given as a string
                              - needs to have only 13 digits
                              - raises an error if it is not formed only with digits or if it does have 13 digits
    -disease- the patient's disease
            - given as a string
    '''

    def __init__(self, first_name, last_name, personal_numerical_code, disease):
        '''
        Constructor for an object of type patient
        '''
        self.__f_name=first_name
        self.__l_name=last_name
        
        if len(str(personal_numerical_code))!=13:
            raise ValueError("The presonal numerical code has to be formed with 13 digits")
        
        self.__num_code=personal_numerical_code
        self.__disease=disease
    
    def __str__(self):
        return "Patient with numerical code " + str(self.__num_code) + " has first name " + str(self.__f_name) +" last name " + str(self.__l_name) + " and disease " +str(self.__disease)
    
    def get_first_name(self):
        '''
        Getter for the atribute first_name of the patient
        ''' 
        return self.__f_name
    
    def set_first_name(self, new_f_name):
        '''
        Setter for the atribute first_name of the patient
        '''
        self.__f_name=new_f_name
        
    def get_last_name(self):
        '''
        Getter for the atribute last_name of the patient
        ''' 
        return self.__l_name
    
    def set_last_name(self, new_l_name):
        '''
        Setter for the atribute last_name of the patient
        '''
        self.__l_name=new_l_name

    def get_personal_numerical_code(self):
        '''
        Getter for the atribute personal_numerical_code of the patient
        ''' 
        return self.__num_code
    
    def set_personal_numerical_code(self, new_personal_numerical_code):
        '''
        Setter for the atribute personal_numerical_code of the patient
        '''
        if  len(str(new_personal_numerical_code))!=13:
            raise ValueError("The presonal numerical code has to be formed with 13 digits")

        self.__num_code=new_personal_numerical_code
    
    def get_disease(self):
        '''
        Getter for the atribute disease of the patient
        ''' 
        return self.__disease
    
    def set_disease(self, new_disease):
        '''
        Setter for the atribute disease of the patient
        '''
        self.__disease=new_disease
    
    def get_age(self):
        '''
        Returns the age of the patient
        '''
        if str(self.__num_code)[0] == '1' or str(self.__num_code)[0] == '2':
            first_two_year="19"
        else:
            first_two_year="20"
        pnc_num=str(self.__num_code)[1]+str(self.__num_code)[2]
        
        year=first_two_year + pnc_num
        
        age=2020-int(year)
        
        return age