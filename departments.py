'''
Created on 10 Dec 2020

@author: Customer
'''
from domain.patients import MyPatients
from utils.sort_and_search import my_sort,my_search

class MyDepartments(object):
    '''
    An object of type MyDepatrments contains:
    - id - the id of the departments 
         - given as a string
         - has to be unique
    - name- the name of the department
          - given as a string
    - number of beds - the number of beds that the department has
                     - given as a number
                     - raises an error if it is not a number
    - a list of objects of type My patinets - it is created in the class
                                            - the patients must have diffrent personal numerical codes
                                            - raises an error if the personal numerical code of a patients is already in the list of patients
    -
    '''

    def __init__(self, department_id, department_name, department_number_of_beds):
        '''
        Constructor for the class MyDepartments
        Raises an error if department_name<0
        '''
        self.__dep_id=department_id
        self.__dep_name=department_name
        
        if department_number_of_beds <0:
            raise ValueError ("The number of beds has to be a positive number")
        
        self.__dep_nr_beds=department_number_of_beds
            
        self.__patients_list=[]
    
    def __len__(self):
        '''
        Returns the lenght of the patients list
        '''
        return len(self.__patients_list)
    
    def __str__(self):
        '''
        Creates a string version of the patients list
        '''
        
        if len(self.__patients_list)==0:
            s=" and no patients"
        else:
            s=" and the following patients:"+'\n' 
            for v in self.__patients_list:
                s+= str(v)+'\n'
        
        return "Department of id " + str(self.__dep_id) + " of name " + str(self.__dep_name) + " has " + str(self.__dep_nr_beds) +" beds"+ s
       
    def add_patient(self, patient:MyPatients):
        '''
        Adds an objet of type MyPatients to the list of patients of a department 
        Raises an error if the object given has a personal numerical code that is already in the list
        '''
        for p in self.__patients_list:
            if patient.get_personal_numerical_code() == p.get_personal_numerical_code():
                raise ValueError ("The personal numerical code of the patients has to be diffrent") 
        
        self.__patients_list.append(patient)
    
    def get_department_id(self):
        '''
        Getter for the atribute departament id of the department   
        '''
        return self.__dep_id
    
    def set_department_id(self, new_department_id):
        '''
        Setter for the atribute departament id of the department   
        '''
        self.__dep_id = new_department_id 
    
    def get_department_name(self):
        '''
        Getter for the atribute department name of the department   
        '''
        return self.__dep_name
    
    def set_department_name(self, new_department_name):
        '''
        Setter for the atribute department name of the department   
        '''
        self.__dep_name = new_department_name 
    
    def get_department_number_of_beds(self):
        '''
        Getter for the atribute department number of beds of the department   
        '''
        return self.__dep_nr_beds
    
    def set_department_number_of_beds(self, new_department_number_of_beds):
        '''
        Setter for the atribute department number of beds of the department  
        Raises an error if the new number of beds is <0
 
        '''
        if new_department_number_of_beds <0:
            raise ValueError ("The number of beds has to be a positive number")

        self.__dep_nr_beds = new_department_number_of_beds
        
    def get_list_of_patients(self):
        '''
        Getter method for the list of patients
        '''
        return self.__patients_list
    
    def set_list_of_patients(self,new_patients_list):
        '''
        Setter for the list of patients
        '''
        self.__patients_list=new_patients_list
        
    def get_at_index(self, index):
        '''
        Getter method: Gets the patient located at a given index
        index- has to be greater or equal to 0 and less than the number of elements in the list
        Raises an error if the the list is empty oor if index is less than 0 or greater than the elemnets in the list

        '''
        if len(self.__patients_list) == 0:
            raise ValueError("The patients list is empty")
        elif index<0 or index>=len(self.__patients_list):
            raise IndexError("The index given does not exist")
        return self.__patients_list[index]
    
    def delete_by_index(self,index):
        '''
        Deletes a patient at a given index
        index- has to be greater or equal to 0 and less than the number of elements in the list
        Raises an error if the the list is empty oor if index is less than 0 or greater than the elemnets in the list

        '''
        if len(self.__patients_list)==0:
            raise ValueError("The list is empty")
        if index<0 or index>=len(self.__patients_list):
            raise IndexError("The index given does not exist")
        del(self.__patients_list[index])

    def update_patient_at_a_given_index(self,index, new_first_name, new_last_name, new_disease):
        '''
        Updates a patient at a given index
        index- has to be greater or equal to 0 and less than the number of elements in the list
        new_first_name- the new first name for the patient
                      - given as a string
        new_last_name- the new last name for the patient
                     - given as a string
        new_disease- the patient's updated disease
                   - given as a string
        new_personal_numerical_code- the patient's updated personal numerical code
                   - given as 13 numbers
        Returns the updated patient
        Raises an error if the the list is empty or if index is less than 0 or greater than the elemnets in the list

        '''
        if len(self.__patients_list)==0:
            raise ValueError("The list is empty")
        
        elif index<0 or index>=len(self.__patients_list):
            raise IndexError("The index given does not exist")   
             
        
        self.__patients_list[index].set_first_name(new_first_name)
        self.__patients_list[index].set_last_name(new_last_name)
        self.__patients_list[index].set_disease(new_disease)

        return self.__patients_list[index]
    
    def sort_patients_by_personal_numerical_code(self):
        ''' 
        Sorts the patients by the personal numerical code modifying the list of patients
        Returns the patients list sorted
        Raises an error if the the list is empty 

        '''
        if len(self.__patients_list)==0:
            raise ValueError ("The list of patients is empty")
        
        my_sort(self.__patients_list, lambda x,y: x.get_personal_numerical_code()<y.get_personal_numerical_code() )      
    
    def sort_patients_alphabetically(self):
        ''' 
        Sorts the patients alphabetically modifying the list of patients
        Raises an error if the list is empty
        '''

        if len(self.__patients_list)==0:
            raise ValueError ("The list of patients is empty")

        my_sort(self.__patients_list, lambda x,y: x.get_last_name() < y.get_last_name())
        
    def get_number_of_patients_of_age_greater_than(self,age):
        '''
        Getter the number of patients with age greater than age
        age- a number greater than 0
        Returns the number of patients with age greater than age
        '''
        nr=0
        for p in self.__patients_list:
            if p.get_age()>age:
                nr+=1
        return nr
    
    def department_has_patients_age_less_than(self,age):
        '''
        Checks if the department has patients with age less than age
        age- a number greater than 0
        Returns True of the department has patients with age less than age and False othewise
        '''
        if len(self.__patients_list)==0:
            return False
        
        if my_search(self.__patients_list, lambda x: x.get_age() < age) !=[]:
            return True
        else:
            return False 
    
    def patients_with_given_string(self,cuv):
        '''
        Checks if the department has patients with given string in their first name or last name
        cuv- the string we are looking for
        Raises an error if the list is empty
        Returns the list of patients with given string in their first name or last name or None if there are no patients with given string in their first name or last name
        '''
        
        if len(self.__patients_list)==0:
            raise ValueError ("The list of patients is empty")    
        
        a=my_search(self.__patients_list, lambda x: (cuv in x.get_first_name()) or (cuv in x.get_last_name()))
        if len(a)==0:
            return None
        else:
            return a
        
    def patients_with_given_name(self,name):
        
        '''
        Checks if the department has patients with given first name 
        name- the first name we are looking for
        Returns True if there are patients with given first name or None False otherwise
        '''

        if len(self.__patients_list)==0:
            return False    
        
        a= my_search(self.__patients_list, lambda x: x.get_first_name() == name)
        if len(a)==0: 
            return False
        else:
            return True                

