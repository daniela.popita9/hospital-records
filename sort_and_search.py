'''
Created on 17 Dec 2020

@author: Customer
'''

def my_search(lst,condition):
    '''
    The universal search algorithm
    condition: the condition for the search
    list- the list where the search is done
    Returns a list of objects that fulfill the condition given
    '''
    result=[]
    for i in range(len(lst)):
        if condition(lst[i]):
            result.append(lst[i])
    return result

def my_sort(lst, relation):
    '''
    The universal sort algorithm
    condition: the condition for the sort
    list- the list where the sort is done
    Returns the list sorted according to the condition
    '''
    for i in range(len(lst)-1):
        for j in range(i+1, len(lst)):
            if not relation(lst[i],lst[j]):
                lst[i], lst[j]=lst[j], lst[i]
    return lst